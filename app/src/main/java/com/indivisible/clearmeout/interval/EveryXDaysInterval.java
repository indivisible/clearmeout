package com.indivisible.clearmeout.interval;


import android.preference.Preference;
import android.util.Log;
import com.indivisible.clearmeout.data.Interval;
import com.indivisible.clearmeout.data.IntervalType;

/**
 * Interval for triggering Profile once Every X Minutes.
 */
public class EveryXDaysInterval
        extends Interval
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private int days = -1;
    private String timeOfDay;

    public static final int DATA_INDEX_DAYS = 0;
    public static final int DATA_INDEX_TIME = 1;
    private static final String TAG = "EveryXDays";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public EveryXDaysInterval()
    {
        super();
        this.setIntervalType(IntervalType.EveryXHours);
    }

    public EveryXDaysInterval(long parentProfileId, boolean isStrictAlarm, boolean isActive,
            long lastRunMillis, String[] data)
    {
        super(parentProfileId, IntervalType.EveryXHours, isStrictAlarm, isActive,
                lastRunMillis, data);
        this.days = daysFromData(data);
        this.timeOfDay = timeFromData(data);
    }

    public EveryXDaysInterval(long id, long parentProfileId, boolean isStrictAlarm,
            boolean isActive, long lastRunMillis, String[] data)
    {
        super(id, parentProfileId, IntervalType.EveryXHours, isStrictAlarm, isActive,
                lastRunMillis, data);
        this.days = daysFromData(data);
        this.timeOfDay = timeFromData(data);
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    public int getDays()
    {
        return this.days;
    }

    public void setDays(int days)
    {
        this.days = days;
        super.setData(dataFromDetails(days, this.timeOfDay));
    }

    public String getTimeOfDay()
    {
        return timeOfDay;
    }

    public void setTimeOfDay(String time)
    {
        this.timeOfDay = time;
        super.setData(dataFromDetails(this.days, time));
    }


    ///////////////////////////////////////////////////////
    ////    next run
    ///////////////////////////////////////////////////////

    @Override
    public long getNextRunMillis(boolean firstRun)
    {
        if (this.days <= 0)
        {
            Log.e(TAG, "(getNextRunMillis) hours not set!");
            return -1L;
        }

        long nextRun = -1L;
        //TODO: calendar calculations
        return nextRun;
    }

    //    private long getNextRunFromNow()
    //    {
    //        long now = System.currentTimeMillis();
    //        long hoursAsMillis = (long) this.getHours() * MILLIS_PER_HOUR;
    //        return now + hoursAsMillis;
    //    }
    //
    //    private long getNextRunFromThen(long lastRun)
    //    {
    //        long now = System.currentTimeMillis();
    //        long hoursAsMillis = (long) this.getHours() * MILLIS_PER_HOUR;
    //        long tryNextRun = lastRun + hoursAsMillis;
    //        if (tryNextRun - now > 0L)
    //        {
    //            return tryNextRun;
    //        }
    //        else
    //        {
    //            return -1;
    //        }
    //    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (this.days <= 0)
        {
            Log.w(TAG, "(persist) hours invalid: " + this.days);
            return false;
        }
        else if (this.timeOfDay == null)
        {
            //TODO: validate time format
            return false;
        }
        else
        {
            return super.canPersistChanges();
        }
    }


    ///////////////////////////////////////////////////////
    ////    preferences
    ///////////////////////////////////////////////////////

    @Override
    public Preference[] makePreferences()
    {
        //TODO: implement makePreferences()
        return null;
    }


    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private static int daysFromData(String[] data)
    {
        int days = -1;
        try
        {
            days = Integer.parseInt(data[DATA_INDEX_DAYS]);
        }
        catch (IndexOutOfBoundsException e)
        {
            Log.e(TAG, "(daysFromData) data array empty!");
        }
        catch (NumberFormatException e)
        {
            Log.e(TAG, "(daysFromData) failed to parse String for int: "
                    + data[DATA_INDEX_DAYS]);
        }
        return days;
    }

    private static String timeFromData(String[] data)
    {
        String time = "";
        try
        {
            time = data[DATA_INDEX_TIME];
        }
        catch (IndexOutOfBoundsException e)
        {
            Log.e(TAG, "(timeFromData) data array incomplete!");
        }
        return time;
    }

    private static String[] dataFromDetails(int days, String time)
    {
        String[] dataArray = new String[] {
                null, null
        };
        if (days > 0)
        {
            dataArray[DATA_INDEX_DAYS] = Integer.toString(days);
        }
        //TODO: validate time format
        dataArray[DATA_INDEX_TIME] = time;
        return dataArray;
    }

}
