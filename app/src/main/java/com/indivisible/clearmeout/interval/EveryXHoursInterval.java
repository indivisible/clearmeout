package com.indivisible.clearmeout.interval;

import android.preference.Preference;
import android.util.Log;
import com.indivisible.clearmeout.data.Interval;
import com.indivisible.clearmeout.data.IntervalType;

/**
 * Interval for triggering Profile once Every X Minutes.
 */
public class EveryXHoursInterval
        extends Interval
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private int hours = -1;

    public static final long MILLIS_PER_HOUR = 3600000L;
    private static final String TAG = "EveryXHours";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public EveryXHoursInterval()
    {
        super();
        this.setIntervalType(IntervalType.EveryXHours);
    }

    public EveryXHoursInterval(long parentProfileId, boolean isStrictAlarm, boolean isActive,
            long lastRunMillis, String[] data)
    {
        super(parentProfileId, IntervalType.EveryXHours, isStrictAlarm, isActive,
                lastRunMillis, data);
        this.hours = hoursFromData(data);
    }

    public EveryXHoursInterval(long id, long parentProfileId, boolean isStrictAlarm,
            boolean isActive, long lastRunMillis, String[] data)
    {
        super(id, parentProfileId, IntervalType.EveryXHours, isStrictAlarm, isActive,
                lastRunMillis, data);
        this.hours = hoursFromData(data);
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    public int getHours()
    {
        return this.hours;
    }

    public void setHours(int hours)
    {
        this.hours = hours;
        super.setData(dataFromHours(hours));
    }


    ///////////////////////////////////////////////////////
    ////    next run
    ///////////////////////////////////////////////////////

    @Override
    public long getNextRunMillis(boolean firstRun)
    {
        if (this.hours <= 0)
        {
            Log.e(TAG, "(getNextRunMillis) hours not set!");
            return -1L;
        }

        long nextRun = -1L;
        long lastRun = this.getLastRunMillis();
        if (!firstRun && lastRun > 0)
        {
            // repeating alarm, set based of last run
            nextRun = getNextRunFromThen(lastRun);
        }
        if (nextRun <= 0L)
        {
            // if never run or isFirstRun
            nextRun = getNextRunFromNow();
        }
        Log.i(TAG, "(nextRun) nextRun calculated as: " + nextRun);
        return nextRun;
    }

    private long getNextRunFromNow()
    {
        long now = System.currentTimeMillis();
        long hoursAsMillis = (long) this.getHours() * MILLIS_PER_HOUR;
        return now + hoursAsMillis;
    }

    private long getNextRunFromThen(long lastRun)
    {
        long now = System.currentTimeMillis();
        long hoursAsMillis = (long) this.getHours() * MILLIS_PER_HOUR;
        long tryNextRun = lastRun + hoursAsMillis;
        if (tryNextRun - now > 0L)
        {
            return tryNextRun;
        }
        else
        {
            return -1;
        }
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (this.hours <= 0)
        {
            Log.w(TAG, "(persist) hours invalid: " + this.hours);
            return false;
        }
        else
        {
            return super.canPersistChanges();
        }
    }


    ///////////////////////////////////////////////////////
    ////    preferences
    ///////////////////////////////////////////////////////

    @Override
    public Preference[] makePreferences()
    {
        //TODO: implement makePreferences()
        return null;
    }


    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private static int hoursFromData(String[] data)
    {
        int hrs = -1;
        try
        {
            hrs = Integer.parseInt(data[0]);
        }
        catch (IndexOutOfBoundsException e)
        {
            Log.e(TAG, "(hoursFromData) data array empty!");
        }
        catch (NumberFormatException e)
        {
            Log.e(TAG, "(hoursFromData) failed to parse String for int: " + data[0]);
        }
        return hrs;
    }

    private static String[] dataFromHours(int hrs)
    {
        String[] dataArray = new String[] {
            null
        };
        if (hrs > 0)
        {
            dataArray[0] = Integer.toString(hrs);
        }
        return dataArray;
    }

}
