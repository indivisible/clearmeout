package com.indivisible.clearmeout.interval;

import android.util.Log;
import com.indivisible.clearmeout.data.Interval;
import com.indivisible.clearmeout.data.IntervalType;

/**
 * Convenience class for handling the multitude of Intervals.
 */
public class IntervalCreator
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private static final String TAG = "IntervalCreator";


    ///////////////////////////////////////////////////////
    ////    static methods
    ///////////////////////////////////////////////////////

    public static Interval newInterval(IntervalType intervalType)
    {
        switch (intervalType)
        {
        // every x
            case EveryXMinutes:
                return new EveryXMinutesInterval();
            case EveryXHours:
                return new EveryXHoursInterval();
            case EveryXDays:
                return new EveryXDaysInterval();

                // simple calendar repeat
            case Daily:
                return new DailyInterval();
            case Weekly:
                return new WeeklyInterval();

                // custom calendar repeat
            case OnTheseWeekdays:
                return new OnTheseWeekdaysInterval();
            case OnTheseDates:
                return new OnTheseDatesInterval();

                // invalid
            case INVALID:
            default:
                Log.e(TAG, "(newInterval) Invalid IntervalType Interval requested: "
                        + intervalType.name());
                return null;
        }
    }
}
