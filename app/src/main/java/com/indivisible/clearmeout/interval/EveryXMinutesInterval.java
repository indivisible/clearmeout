package com.indivisible.clearmeout.interval;

import android.content.Context;
import android.preference.Preference;
import android.util.Log;
import com.indivisible.clearmeout.data.Interval;
import com.indivisible.clearmeout.data.IntervalType;
import com.indivisible.clearmeout.preferences.NumberPreference;

/**
 * Interval for triggering Profile once Every X Minutes.
 */
public class EveryXMinutesInterval
        extends Interval
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private int minutes = -1;

    public static final long MILLIS_PER_MIN = 60000L;
    private static final String TAG = "EveryXMins";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public EveryXMinutesInterval()
    {
        super();
        this.setIntervalType(IntervalType.EveryXMinutes);
    }

    public EveryXMinutesInterval(long parentProfileId, boolean isStrictAlarm,
            boolean isActive, long lastRunMillis, String[] data)
    {
        super(parentProfileId, IntervalType.EveryXMinutes, isStrictAlarm, isActive,
                lastRunMillis, data);
        loadFromData();
    }

    public EveryXMinutesInterval(long id, long parentProfileId, boolean isStrictAlarm,
            boolean isActive, long lastRunMillis, String[] data)
    {
        super(id, parentProfileId, IntervalType.EveryXMinutes, isStrictAlarm, isActive,
                lastRunMillis, data);
        loadFromData();
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    public int getMinutes()
    {
        return this.minutes;
    }

    public void setMinutes(int minutes)
    {
        this.minutes = minutes;
        saveToData();
    }


    ///////////////////////////////////////////////////////
    ////    next run
    ///////////////////////////////////////////////////////

    @Override
    public long getNextRunMillis(boolean firstRun)
    {
        if (this.minutes <= 0)
        {
            Log.e(TAG, "(getNextRunMillis) minutes not set!");
            return -1L;
        }

        long nextRun = -1L;
        long lastRun = this.getLastRunMillis();
        if (!firstRun && lastRun > 0)
        {
            // repeating alarm, set based of last run
            nextRun = getNextRunFromThen(lastRun);
        }
        if (nextRun <= 0L)
        {
            // if never run or isFirstRun
            nextRun = getNextRunFromNow();
        }
        Log.i(TAG, "(nextRun) nextRun calculated as: " + nextRun);
        return nextRun;
    }

    private long getNextRunFromNow()
    {
        long now = System.currentTimeMillis();
        long minsAsMillis = MILLIS_PER_MIN * this.minutes;
        return now + minsAsMillis;
    }

    private long getNextRunFromThen(long lastRun)
    {
        long now = System.currentTimeMillis();
        long minsAsMillis = MILLIS_PER_MIN * this.minutes;
        long tryNextRun = lastRun + minsAsMillis;

        if (tryNextRun - now > Interval.MINIMUM_TIME_TO_TRIGGER)
        {
            return tryNextRun;
        }
        else
        {
            return now + minsAsMillis;
        }
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (this.minutes <= 0)
        {
            Log.w(TAG, "(persist) minutes invalid: " + this.minutes);
            return false;
        }
        else
        {
            return super.canPersistChanges();
        }
    }


    ///////////////////////////////////////////////////////
    ////    preferences
    ///////////////////////////////////////////////////////

    @Override
    public Preference[] makePreferences(Context context)
    {
        Preference[] prefs = new Preference[1];
        NumberPreference minutesPref = new NumberPreference(context);
        minutesPref.setNumber(this.minutes);
        minutesPref.setTitle("Test title: Every X Minutes");
        minutesPref.setSummary("test summary: %1$s minutes");
        return prefs;
    }


    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    @Override
    public void loadFromData()
    {
        int mins = -1;
        try
        {
            mins = Integer.parseInt(getData()[0]);
        }
        catch (IndexOutOfBoundsException e)
        {
            Log.e(TAG, "(minsFromData) data array empty!");
        }
        catch (NumberFormatException e)
        {
            Log.e(TAG, "(minsFromData) failed to parse String for int: " + getData()[0]);
        }
        this.minutes = mins;
    }

    @Override
    public void saveToData()
    {
        String[] dataArray = new String[] {
                null, null
        };
        if (this.minutes > 0)
        {
            dataArray[0] = Integer.toString(minutes);
        }
        setData(dataArray);
    }

}
