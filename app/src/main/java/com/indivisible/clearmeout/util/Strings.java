package com.indivisible.clearmeout.util;

/**
 * Created by indiv on 19/06/14.
 */
public class Strings
{

    // preference keys - defined in preference_keys.xml for use in preference_X.xml

    // keys for intents
    public static final String INTENT_KEY_FILTERS = "filters";
    public static final String INTENT_KEY_INTERVALS = "intervals";

    // extras keys
    public static final String EXTRA_PROFILEID = "profile_id";
    public static final String EXTRA_CHILD_TYPE = "profile_child_type";

    // interval keys
    public static final String INTERVAL_XMINS = "interval_every_x_mins";
    public static final String INTERVAL_XHOURS = "interval_every_x_hours";
    public static final String INTERVAL_XDAYS = "interval_every_x_days";
    public static final String INTERVAL_DAILY = "interval_daily";
    public static final String INTERVAL_WEEKLY = "interval_weekly";
    public static final String INTERVAL_THESE_WEEKDAYS = "interval_on_these_weekdays";
    public static final String INTERVAL_THESE_DATES = "interval_on_these_dates";
    public static final String[] INTERVALS_ALL_KEYS = new String[] {
            INTERVAL_XMINS, INTERVAL_XHOURS, INTERVAL_XDAYS, INTERVAL_DAILY, INTERVAL_WEEKLY,
            INTERVAL_THESE_WEEKDAYS, INTERVAL_THESE_DATES
    };

    // filter keys
    public static final String FILTER_FILE_EXTENSION = "filter_file_extension";
    public static final String FILTER_FILE_NAME = "filter_file_name";
    public static final String FILTER_FILE_SIZE = "filter_file_size";
    public static final String FILTER_DIR_NAME = "filter_dir_name";
    public static final String FILTER_DATE_CREATED = "filter_date_created";
    public static final String FILTER_DATE_MODIFIED = "filter_date_modified";
    public static final String FILTER_DATE_ACCESSED = "filter_date_accessed";
    public static final String[] FILTERS_ALL_KEYS = new String[] {
            FILTER_FILE_EXTENSION, FILTER_FILE_NAME, FILTER_FILE_SIZE, FILTER_DIR_NAME,
            FILTER_DATE_CREATED, FILTER_DATE_MODIFIED, FILTER_DATE_ACCESSED
    };

}
