package com.indivisible.clearmeout.preferences;

/**
 * This class borrowed and modified from CommonsWare's
 * (http://stackoverflow.com/users/115145/commonsware) solution to
 * "http://stackoverflow.com/questions/5533078/timepicker-in-preferencescreen"
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;

public class TimePreference
        extends DialogPreference
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private int lastHour = 0;
    private int lastMinute = 0;
    private String summaryString = null;
    private boolean doFormatSummary = false;
    private TimePicker picker = null;

    private static final String TIME_SEPERATOR = ":";
    private static final String DEFAULT_TIME = "02:00";
    private static final String STRING_REPLACE = "%1$s";
    private static final String TAG = "TimePref";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public TimePreference(Context ctxt)
    {
        this(ctxt, null);
    }

    public TimePreference(Context ctxt, AttributeSet attrs)
    {
        this(ctxt, attrs, android.R.attr.preferenceStyle);
    }

    public TimePreference(Context ctxt, AttributeSet attrs, int defStyle)
    {
        super(ctxt, attrs, defStyle);
        setPositiveButtonText("Set");
        setNegativeButtonText("Cancel");
        setDefaultValue(DEFAULT_TIME);
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    // integer time

    public int getHour()
    {
        return this.lastHour;
    }

    public void setHour(int hour)
    {
        this.lastHour = hour;
    }

    public int getMinute()
    {
        return this.lastMinute;
    }

    public void setMinute(int minute)
    {
        this.lastMinute = minute;
    }

    public int[] getTime()
    {
        return new int[] {
                this.lastHour, this.lastMinute
        };
    }

    public void setTime(int[] timeArray)
    {
        if (timeArray.length == 2)
        {
            this.lastHour = timeArray[0];
            this.lastMinute = timeArray[1];
        }
    }

    public void setTime(String timeString)
    {
        int[] timeArray = parseStringForTime(timeString);
        this.lastHour = timeArray[0];
        this.lastMinute = timeArray[1];
    }

    // custom summary

    @Override
    public void setSummary(CharSequence summary)
    {
        this.summaryString = summary.toString();
        if (summaryString.contains(STRING_REPLACE))
        {
            doFormatSummary = true;
        }
        else
        {
            doFormatSummary = false;
        }
        updateSummary();
    }

    public void updateSummary()
    {
        if (doFormatSummary)
        {
            super.setSummary(String.format(summaryString, this.toString()));
        }
        else
        {
            super.setSummary(summaryString);
        }
    }

    @Override
    public String toString()
    {
        return String.format("%02d", lastHour) + ":" + String.format("%02d", lastMinute);
    }


    ///////////////////////////////////////////////////////
    ////    dialog
    ///////////////////////////////////////////////////////

    @Override
    protected View onCreateDialogView()
    {
        picker = new TimePicker(getContext());
        picker.setIs24HourView(true);
        picker.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        return (picker);
    }

    @Override
    protected void onBindDialogView(View v)
    {
        super.onBindDialogView(v);
        picker.setCurrentHour(lastHour);
        picker.setCurrentMinute(lastMinute);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult)
    {
        super.onDialogClosed(positiveResult);

        if (positiveResult)
        {
            lastHour = picker.getCurrentHour();
            lastMinute = picker.getCurrentMinute();
            updateSummary();
        }
    }


    ///////////////////////////////////////////////////////
    ////    persistence
    ///////////////////////////////////////////////////////

    //ASK: need these methods as we persist manually elsewhere?

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index)
    {
        return (a.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue)
    {
        String time = null;

        if (restoreValue)
        {
            if (defaultValue == null)
            {
                time = getPersistedString("00:00");
            }
            else
            {
                time = getPersistedString(defaultValue.toString());
            }
        }
        else
        {
            time = defaultValue.toString();
        }
        this.setTime(time);
    }


    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////

    private int[] parseStringForTime(String timeString)
    {
        int[] timeArray = new int[2];
        String[] timeParts = timeString.split(TIME_SEPERATOR);
        try
        {
            timeArray[0] = Integer.parseInt(timeParts[0]);
            timeArray[1] = Integer.parseInt(timeParts[1]);
        }
        catch (NumberFormatException e)
        {
            Log.e(TAG, "(parseStrForTime) Error parsing String time for ints: " + timeString);
        }
        catch (IndexOutOfBoundsException e)
        {
            Log.e(TAG, "(parseStrForTime) Error splitting String to parse for int times: "
                    + timeString);
        }
        return timeArray;
    }


}
