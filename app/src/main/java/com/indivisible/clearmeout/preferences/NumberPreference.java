package com.indivisible.clearmeout.preferences;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;


public class NumberPreference
        extends DialogPreference
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private int lastValue = -1;
    private String summaryString = null;
    private NumberPicker picker = null;
    private boolean doFormatSummary;

    private static final String STRING_REPLACE = "%1$s";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    public NumberPreference(Context context)
    {
        this(context, null);
    }

    public NumberPreference(Context context, AttributeSet attrs)
    {
        this(context, attrs, android.R.attr.preferenceStyle);
    }

    public NumberPreference(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setPositiveButtonText(context.getString(android.R.string.ok));
        setNegativeButtonText(context.getString(android.R.string.cancel));
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    // preference value

    public void setNumber(int number)
    {
        this.lastValue = number;
    }

    public int getNumber()
    {
        return lastValue;
    }

    // displayed text

    @Override
    public void setSummary(CharSequence summary)
    {
        this.summaryString = summary.toString();
        if (summaryString.contains(STRING_REPLACE))
        {
            doFormatSummary = true;
        }
        else
        {
            doFormatSummary = false;
        }
        updateSummary();
    }

    @Override
    public CharSequence getSummary()
    {
        if (summaryString != null)
        {
            return String.format(summaryString, this.toString());
        }
        return super.getSummary();
    }

    public void updateSummary()
    {
        if (doFormatSummary)
        {
            super.setSummary(String.format(this.summaryString, this.lastValue));
        }
        else
        {
            super.setSummary(this.summaryString);
        }
    }


    @Override
    public String toString()
    {
        return String.valueOf(lastValue);
    }


    ///////////////////////////////////////////////////////
    ////    dialog
    ///////////////////////////////////////////////////////

    @Override
    protected View onCreateDialogView()
    {
        picker = new NumberPicker(getContext());
        //picker.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        return (picker);
    }

    @Override
    protected void onBindDialogView(View v)
    {
        super.onBindDialogView(v);
        picker.setValue(lastValue);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult)
    {
        super.onDialogClosed(positiveResult);
        if (positiveResult)
        {
            lastValue = picker.getValue();
            updateSummary();
        }
    }

}
