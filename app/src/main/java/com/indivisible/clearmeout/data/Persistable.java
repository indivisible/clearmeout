package com.indivisible.clearmeout.data;

/**
 * Abstract class for objects that should be saved to persisted storage.
 */
public abstract class Persistable
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private boolean modified;
    private boolean deleted;

    private static final String TAG = "Persistable";


    ///////////////////////////////////////////////////////
    ////    implemented methods
    ///////////////////////////////////////////////////////

    /**
     * Check whether this object has been flagged as having been changed.
     *
     * @return
     */
    public boolean isModified()
    {
        return this.modified;
    }

    /**
     * Set whether this object has been changed.
     *
     * @param wasModified
     */
    public void setModified(boolean wasModified)
    {
        this.modified = wasModified;
    }

    /**
     * Check whether this object has been marked to be deleted.
     *
     * @return
     */
    public boolean isMarkedForDeletion()
    {
        return this.deleted;
    }

    /**
     * Set whether this object should be deleted.
     *
     * @param markForDeletion
     */
    public void setDeletable(boolean markForDeletion)
    {
        this.deleted = markForDeletion;
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    /**
     * Check whether or not this object is in a valid state to be persisted to
     * storage.
     *
     * @return Whether save was completed successfully.
     */
    public boolean canPersistChanges()
    {
        //REM: Test for isModified way back down with first canPersist check
        return true;
    }

    //ASK: need abandon changes or just reload the list/object?
    //public void abandonChanges();
}
