package com.indivisible.clearmeout.data;

import java.util.Arrays;
import android.util.Log;

/**
 * Abstract class to be used for various Filters to be used when parsing folders
 * for candidates for deletion.
 */
public abstract class Filter
        extends ProfileChild
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private FilterType filterType;
    private boolean whitelist;

    private static final String TAG = "Filter";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    /**
     * Create a new, default, Filter object. <br/>
     * Should be populated immediately as almost all functionality depends on
     * valid values.
     */
    public Filter()
    {
        this(-1L, FilterType.INVALID, false, false, new String[] {
            "NO DATA"
        });
    }

    /**
     * Create a new Filter object that has not yet been persisted to storage. <br/>
     * i.e. Has no id/primary key.
     *
     * @param parentProfileId
     * @param filterType
     * @param isActive
     * @param isWhitelist
     * @param data
     */
    public Filter(long parentProfileId, FilterType filterType, boolean isActive,
            boolean isWhitelist, String[] data)
    {
        this(-1L, parentProfileId, filterType, isActive, isWhitelist, data);
    }

    /**
     * Create a new Interval object that has been read from or capable of being
     * saved to persisted storage.
     *
     * @param id
     * @param parentProfileId
     * @param filterType
     * @param isActive
     * @param isWhitelist
     * @param data
     */
    public Filter(long id, long parentProfileId, FilterType filterType, boolean isActive,
            boolean isWhitelist, String[] data)
    {
        this.setId(id);
        this.setParentProfileId(parentProfileId);
        this.filterType = filterType;
        this.setActive(isActive);
        this.whitelist = isWhitelist;
        this.setData(data);
    }


    ///////////////////////////////////////////////////////
    ////    get & set
    ///////////////////////////////////////////////////////

    /**
     * Get the FilterType that this Filter has been configured as.
     *
     * @return
     */
    public FilterType getFilterType()
    {
        return filterType;
    }

    /**
     * Set the FilterType that this Filter has been configured as.
     *
     * @param filterType
     */
    public void setFilterType(FilterType filterType)
    {
        this.filterType = filterType;
    }

    /**
     * Check whether the Filter should delete or ignore files/directories that
     * match this Filter.
     *
     * @return true - delete files that DO match <br/>
     *         false - delete files that DO NOT match
     */
    public boolean isWhitelist()
    {
        return whitelist;
    }

    /**
     * Set whether the Filter should delete or ignore files/directories that
     * match this Filter.
     *
     * @param isWhitelist
     *        true to delete files that DO match, false to delete files that DO
     *        NOT match
     */
    public void setWhitelist(boolean isWhitelist)
    {
        this.whitelist = isWhitelist;
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (getFilterType() == FilterType.INVALID)
        {
            Log.e(TAG, "(persist) Cannot save. Invalid FilterType: " + getFilterType().name());
            return false;
        }
        if (getData().length != 1 || getData()[0] == null)
        {
            Log.e(TAG, "(persist) Cannot save. Data null or too long (1): " + getData().length);
            return false;
        }
        return super.canPersistChanges();
    }

    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////

    /**
     * Simple String to identify this Filter. <br/>
     * Uses id/primary key and FilterType.
     *
     * @return
     */
    @Override
    public String toString()
    {
        return this.getId() + ": " + filterType.name();
    }

    /**
     * More detailed String to identify and describe this Filter object. <br/>
     * Uses id/primary key, FilterType and saved data.
     *
     * @return
     */
    public String debugContent()
    {
        return "p_" + this.getParentProfileId() + " / w_" + whitelist + " / a_"
                + this.isActive() + "\n" + Arrays.toString(this.getData());
    }
}
