package com.indivisible.clearmeout.data;

import android.content.Context;
import android.preference.Preference;

/**
 * Abstract class for objects that are linked to a parent Profile.
 */
public abstract class ProfileChild
        extends DatabaseEntry
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private long parentId = -1L;
    private Preference[] preferences = null;
    private boolean active = false;
    private String[] data = null;

    public static final int MAX_DATA_SIZE = 2;
    private static final String TAG = "ProfileChild";


    ///////////////////////////////////////////////////////
    ////    implemented methods
    ///////////////////////////////////////////////////////

    /**
     * Get the Id of the Profile this ProfileChild is attached to. <br/>
     * (parentProfile foreign key)
     *
     * @return -1L if unset <br/>
     *         > 0L if set
     */
    public long getParentProfileId()
    {
        return this.parentId;
    }

    /**
     * Set the Id of the Profile this ProfileChild is attached to. <br/>
     *
     * @param parentProfileId
     *        the parentProfile's foreign key
     */
    public void setParentProfileId(long parentProfileId)
    {
        this.parentId = parentProfileId;
    }

    /**
     * Get the associated Preferences for this ProfileChild object. <br />
     * The number and types will differ for each type. <br/>
     * ASK: need Context?
     *
     * @param context
     * @return
     */
    public Preference[] getPreferences(Context context)
    {
        return this.preferences;
    }

    /**
     * Set the associated Preferences for this ProfileChild object. <br/>
     * The number and types will differ for each type.
     *
     * @param preferences
     */
    public void setPreferences(Preference[] preferences)
    {
        this.preferences = preferences;
    }

    /**
     * Set whether this ProfileChild is active and its attributes should be
     * taken into account for the Profile.
     *
     * @return
     */
    public boolean isActive()
    {
        return this.active;
    }

    /**
     * Set whether this ProfileChild is active and its attributes should be
     * taken into account for the Profile.
     *
     * @param isActive
     */
    public void setActive(boolean isActive)
    {
        this.active = isActive;
    }

    /**
     * Get the String array that contains the raw data for this ProfileChild's
     * configuration. <br />
     * Max length is 4 but may be shorter.
     *
     * @return
     */
    public String[] getData()
    {
        return this.data;
    }

    /**
     * Set the String array that contains the raw data for this ProfileChild's
     * configuration. <br />
     * Max length is 4 but may be shorter.
     *
     * @param data
     */
    public void setData(String[] data)
    {
        this.data = data;
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (this.getParentProfileId() < 0)
        {
            return false;
        }
        else if (this.getData() == null)
        {
            return false;
        }
        else
        {
            return super.canPersistChanges();
        }
    }

    ///////////////////////////////////////////////////////
    ////    abstract methods
    ///////////////////////////////////////////////////////

    /**
     * Save info from database retrieved String[] to localised data for specific
     * ProfileChild.
     */
    public abstract void loadFromData();

    /**
     * Convert all localised data for ProfileChild to generic String[] for
     * saving to database. <br/>
     * Max length is currently 4.
     *
     * @param dataParts
     */
    public abstract void saveToData();

    /**
     * Create an array of Preferences to be used to configure the ProfileChild.
     *
     * @return
     */
    public abstract Preference[] makePreferences(Context context);

}
