package com.indivisible.clearmeout.data;

import java.util.List;

/**
 * Class to hold a Profile object that encapsulates an entire distinct
 * ClearMeOut configuration.
 */
public class Profile
        extends DatabaseEntry
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private String name = null;
    private boolean active = false;

    private Target target = null;
    private List<Interval> intervals = null;
    private List<Filter> filters = null;

    private static final String TAG = "Profile";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    /**
     * Create a new, default Profile object. <br/>
     * Should be populated immediately as almost all functionality depends on
     * valid values.
     */
    public Profile()
    {
        this("NO NAME", false);
    }

    /**
     * Create a new Profile object that has not yet been persisted to storage. <br/>
     * i.e. Has no id/primary key.
     *
     * @param profileName
     * @param isActive
     */
    public Profile(String profileName, boolean isActive)
    {
        this(-1, profileName, isActive);
    }

    /**
     * Create a new Profile object that has been read from or capable of being
     * saved to persisted storage.
     *
     * @param id
     * @param profileName
     * @param isActive
     */
    public Profile(long id, String profileName, boolean isActive)
    {
        this.setId(id);
        this.name = profileName;
        this.active = isActive;
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    /**
     * Get the user saved name for this Profile.
     *
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * Set the user decided name for this Profile.
     *
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Check whether this Profile has been set as active.
     *
     * @return true - this Profile may get triggered. <br/>
     *         false - this Profile will not be automatically triggered.
     */
    public boolean isActive()
    {
        return active;
    }

    /**
     * Set whether this profile is active.
     *
     * @param isActive
     */
    public void setActive(boolean isActive)
    {
        this.active = active;
    }

    /**
     * Get the associated Target object for this Profile.
     *
     * @return
     */
    public Target getTarget()
    {
        return this.target;
    }

    /**
     * Set the associated, associated Target object for this Profile.
     *
     * @param target
     */
    public void setTarget(Target target)
    {
        this.target = target;
    }

    /**
     * Get the stored, associated List of Filters for this Profile.
     *
     * @return
     */
    public List<Filter> getFilters()
    {
        return this.filters;
    }

    /**
     * Set the stored, associated List of Filters for this Profile.
     *
     * @param filters
     */
    public void setFilters(List<Filter> filters)
    {
        this.filters = filters;
    }

    /**
     * Get the stored, associated List of Intervals for this Profile.
     *
     * @return
     */
    public List<Interval> getIntervals()
    {
        return this.intervals;
    }

    /**
     * Set the stored, associated List of Intervals for this Profile.
     *
     * @param intervals
     */
    public void setIntervals(List<Interval> intervals)
    {
        this.intervals = intervals;
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (name == null)
        {
            return false;
        }
        if (target == null)
        {
            return false;
        }
        return super.canPersistChanges();
    }

    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////

    /**
     * Simple String to identify this Profile object. <br/>
     * Uses id/primary key and 'name'.
     *
     * @return
     */
    @Override
    public String toString()
    {
        //TODO: clean up toString to just 'name' later
        return this.getId() + "/" + name;
    }

    /**
     * More detailed String to identify and describe this Profile object. <br/>
     * Uses Target id/primary key, Filter count and Interval count.
     *
     * @return
     */
    public String debugContent()
    {
        return "t_" + target.getId() + " / f_" + filters.size() + " / i_" + intervals.size();
    }

}
