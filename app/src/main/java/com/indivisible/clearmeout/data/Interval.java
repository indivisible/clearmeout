package com.indivisible.clearmeout.data;

/**
 * Abstract class to represent a single Interval for the associated Profile to
 * be run on.
 */
public abstract class Interval
        extends ProfileChild
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    // database fields
    private IntervalType intervalType = IntervalType.INVALID;
    private boolean strictAlarm = false;
    private long lastRunMillis = 0L;

    // do not trigger alarm less in than 10 secs
    public static final long MINIMUM_TIME_TO_TRIGGER = 10000L;
    private static final String TAG = "Interval";

    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    /**
     * Create a new, default, Interval object. <br/>
     * Should be populated immediately as almost all functionality depends on
     * valid values.
     */
    public Interval()
    {
        this(-1L, IntervalType.INVALID, false, false, 0L, new String[0]);
    }

    /**
     * Create a new Interval object that has not yet been persisted to storage. <br/>
     * i.e. Has no id/primary key.
     *
     * @param parentProfileId
     * @param intervalType
     * @param isStrictAlarm
     * @param isActive
     * @param lastRunMillis
     * @param data
     */
    public Interval(long parentProfileId, IntervalType intervalType, boolean isStrictAlarm,
            boolean isActive, long lastRunMillis, String[] data)
    {
        this(-1L, parentProfileId, intervalType, isStrictAlarm, isActive, lastRunMillis, data);
    }

    /**
     * Create a new Interval object that has been read from or capable of being
     * saved to persisted storage.
     *
     * @param id
     * @param parentProfileId
     * @param intervalType
     * @param isStrictAlarm
     * @param isActive
     * @param lastRunMillis
     * @param data
     */
    public Interval(long id, long parentProfileId, IntervalType intervalType,
            boolean isStrictAlarm, boolean isActive, long lastRunMillis, String[] data)
    {
        this.setId(id);
        this.setParentProfileId(parentProfileId);
        this.intervalType = intervalType;
        this.strictAlarm = isStrictAlarm;
        this.setActive(isActive);
        this.lastRunMillis = lastRunMillis;
        this.setData(data);
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    /**
     * Get this Interval's IntervalType.
     *
     * @return
     */
    public IntervalType getIntervalType()
    {
        return intervalType;
    }

    /**
     * Set what type of Interval this represents. <br/>
     * Does not persist the change!
     *
     * @param intervalType
     *        Enum of possible IntervalTypes
     */
    public void setIntervalType(IntervalType intervalType)
    {
        this.intervalType = intervalType;
    }

    /**
     * Check whether this Interval should wake the device to run. <br/>
     * Does not persist the change!
     *
     * @return
     */
    public boolean isStrictAlarm()
    {
        return strictAlarm;
    }

    /**
     * Set whether or not the device should wake to trigger this Interval on
     * it's exact set time.
     *
     * @param isStrictAlarm
     *        true to wake device, false to run when next convenient
     */
    public void setStrictAlarm(boolean isStrictAlarm)
    {
        this.strictAlarm = isStrictAlarm;
    }

    /**
     * Get the last time this Interval was run.
     *
     * @return 0L if unset<br/>
     *         -1L on error
     */
    public long getLastRunMillis()
    {
        return lastRunMillis;
    }

    /**
     * Set the time (long millis) this Interval was last run. <br/>
     * Does not persist the change!
     *
     * @param lastRunMillis
     *        The time (as milliseconds/epoch time) when this Interval was last
     *        triggered.
     */
    public void setLastRunMillis(long lastRunMillis)
    {
        this.lastRunMillis = lastRunMillis;
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (getIntervalType() == IntervalType.INVALID)
        {
            return false;
        }
        else
        {
            return super.canPersistChanges();
        }
    }


    ///////////////////////////////////////////////////////
    ////    abstract methods
    ///////////////////////////////////////////////////////

    /**
     * Get the time when this Interval should be next run.
     *
     * @param firstRun
     *        Whether this scheduled run is the first in a series or a repeat.
     * @return The time (in epoch milliseconds) when this Interval should run
     *         next. <br />
     *         -1L on error <br />
     *         0L if should not be run (i.e. not active)
     */
    public abstract long getNextRunMillis(boolean firstRun);


    ///////////////////////////////////////////////////////
    ////    unimplemented inherited, abstract methods (for ref)
    ///////////////////////////////////////////////////////

    //public abstract Preference[] makePreferences();


    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////

    /**
     * Simple String to identify this Interval. <br/>
     * Uses id/primary key and IntervalType.
     *
     * @return
     */
    @Override
    public String toString()
    {
        return this.getId() + ": " + intervalType.name();
    }

    /**
     * More detailed String to identify and describe this Interval object. <br/>
     * Uses id/primary key, IntervalType, alarm settings and saved data.
     *
     * @return
     */
    public String debugContent()
    {
        return "p_" + this.getParentProfileId() + " / s_" + strictAlarm + " / a_"
                + this.isActive() + "\nr_" + lastRunMillis + " / d_" + this.getData().length;
    }
}
