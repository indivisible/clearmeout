package com.indivisible.clearmeout.data;

import android.preference.Preference;
import android.util.Log;

/**
 * Class to hold information on a Profile's targeted directory.
 */
public class Target
        extends ProfileChild
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private String rootDirectory;
    private boolean recursive;
    private boolean deleteDirectories;

    private static final String DEFAULT_DIR = "NO DIRECTORY";
    private static final String TAG = "Target";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    /**
     * Create a new, default, Target object. <br/>
     * Should be populated immediately as almost all functionality depends on
     * valid values.
     */
    public Target()
    {
        this(-1, DEFAULT_DIR, false, false);
    }

    /**
     * Create a new Target object that has not yet been persisted to storage. <br/>
     * i.e. Has no id/primary key.
     *
     * @param parentProfileId
     * @param rootDirectory
     * @param isRecursive
     * @param doDeleteDirectories
     */
    public Target(long parentProfileId, String rootDirectory, boolean isRecursive,
            boolean doDeleteDirectories)
    {
        this(-1, parentProfileId, rootDirectory, isRecursive, doDeleteDirectories);
    }

    /**
     * Create a new Target object that has been read from or capable of being
     * saved to persisted storage.
     *
     * @param id
     * @param parentProfileId
     * @param rootDirectory
     * @param isRecursive
     * @param doDeleteDirectories
     */
    public Target(long id, long parentProfileId, String rootDirectory, boolean isRecursive,
            boolean doDeleteDirectories)
    {
        this.setId(id);
        this.setParentProfileId(parentProfileId);
        this.rootDirectory = rootDirectory;
        this.recursive = isRecursive;
        this.deleteDirectories = doDeleteDirectories;
    }


    ///////////////////////////////////////////////////////
    ////    gets & sets
    ///////////////////////////////////////////////////////

    /**
     * Get the target directory, the root location to work with.
     *
     * @return String path of the rootDirectory
     */
    public String getRootDirectory()
    {
        return rootDirectory;
    }

    /**
     * Set the target directory, the root location to work with.
     *
     * @param rootDirectory
     */
    public void setRootDirectory(String rootDirectory)
    {
        this.rootDirectory = rootDirectory;
    }

    /**
     * Check whether actions should be performed recursively on the
     * 'rootDirectory'. <br/>
     *
     * @return Whether sub-folders should also be traversed.
     */
    public boolean isRecursive()
    {
        return recursive;
    }

    /**
     * Set whether actions should be performed recursively on the
     * 'rootDirectory'. <br/>
     *
     * @param isRecursive
     *        Whether sub-folders should also be traversed.
     */
    public void setRecursive(boolean isRecursive)
    {
        this.recursive = isRecursive;
    }

    /**
     * Check whether sub-directories should be deleted or left in place (even if
     * empty).
     *
     * @return true - delete empty sub-directories. <br/>
     *         false - leave all sub-directories intact.
     */
    public boolean doDeleteDirectories()
    {
        return deleteDirectories;
    }

    /**
     * Set whether empty sub-directories should be deleted or left in place.
     *
     * @param doDeleteDirectories
     *        Do attempt to delete sub-directories.
     */
    public void setDeleteDirectories(boolean doDeleteDirectories)
    {
        this.deleteDirectories = doDeleteDirectories;
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (getRootDirectory().equals(DEFAULT_DIR))
        {
            Log.e(TAG, "(persist) Cannot save. Default directory text: " + getRootDirectory());
            return false;
        }
        return super.canPersistChanges();
    }


    ///////////////////////////////////////////////////////
    ////    preferences
    ///////////////////////////////////////////////////////

    @Override
    public Preference[] makePreferences()
    {
        //TODO: implement makePreferences()
        return null;
    }


    ///////////////////////////////////////////////////////
    ////    util
    ///////////////////////////////////////////////////////

    /**
     * Simple String to identify this Target object. <br/>
     * Uses id/primary key and 'rootDirectory'.
     *
     * @return
     */
    @Override
    public String toString()
    {
        return this.getId() + ": " + rootDirectory;
    }

    /**
     * More detailed String to identify and describe this Target object. <br/>
     * Uses Target id/primary key, Filter count and Interval count.
     *
     * @return
     */
    public String debugContent()
    {
        return "p_" + this.getParentProfileId() + " / r_" + recursive + " / d_"
                + deleteDirectories;
    }
}
