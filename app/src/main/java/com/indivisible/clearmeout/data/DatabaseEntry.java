package com.indivisible.clearmeout.data;

import android.util.Log;

/**
 * Abstract class for objects that are read from a database
 */
public abstract class DatabaseEntry
        extends Persistable
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private long id = -1L;

    private static final String TAG = "DatabaseEntry";


    ///////////////////////////////////////////////////////
    ////    implemented methods
    ///////////////////////////////////////////////////////

    /**
     * Get the Id (primary key) for this DatabaseEntry.
     *
     * @return -1L if unset <br/>
     *         > 0L if set
     */
    public long getId()
    {
        return id;
    }

    /**
     * Set the Id (primary key) for this DatabaseEntry.
     *
     * @param id
     *        the primary key for the corresponding database row
     */
    public void setId(long id)
    {
        this.id = id;
    }


    ///////////////////////////////////////////////////////
    ////    persist
    ///////////////////////////////////////////////////////

    @Override
    public boolean canPersistChanges()
    {
        if (this.getId() < 0)
        {
            Log.v(TAG, "(persist) new save");
        }
        return super.canPersistChanges();
    }

}
