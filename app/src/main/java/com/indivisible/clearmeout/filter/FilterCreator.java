package com.indivisible.clearmeout.filter;

import android.util.Log;
import com.indivisible.clearmeout.data.Filter;
import com.indivisible.clearmeout.data.FilterType;

/**
 * Convenience class for handling the multitude of Filters.
 */
public class FilterCreator
{

    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    private static final String TAG = "FilterCreator";


    ///////////////////////////////////////////////////////
    ////    static methods
    ///////////////////////////////////////////////////////

    public static Filter newFilter(FilterType filterType)
    {
        switch (filterType)
        {
        // files
            case FILE_NAME:
                return new FileNameFilter();
            case FILE_EXTENSION:
                return new FileExtensionFilter();
            case FILE_SIZE:
                return new FileSizeFilter();

                // directories
            case DIR_NAME:
                return new DirectoryNameFilter();

                // attributes
            case DATE_CREATED:
                return new DateCreatedFilter();
            case DATE_MODIFIED:
                return new DateModifiedFilter();
            case DATE_ACCESSED:
                return new DateAccessedFilter();

                // failed
            case INVALID:
            default:
                Log.e(TAG,
                        "(newFilter) Invalid FilterType Filter requested: "
                                + filterType.name());
                return null;
        }
    }
}
