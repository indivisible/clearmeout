package com.indivisible.clearmeout.filter;

import android.preference.Preference;
import com.indivisible.clearmeout.data.Filter;

public class FileExtensionFilter
        extends Filter
{

    @Override
    public Preference[] makePreferences()
    {
        return new Preference[0];
    }
}
