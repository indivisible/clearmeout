package com.indivisible.clearmeout.filter;

import android.preference.Preference;
import com.indivisible.clearmeout.data.Filter;

/**
 * Created by indiv on 01/07/14.
 */
public class DateCreatedFilter
        extends Filter
{

    @Override
    public Preference[] makePreferences()
    {
        return new Preference[0];
    }
}
